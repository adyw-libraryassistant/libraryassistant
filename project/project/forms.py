from django import forms
from django.contrib.auth.models import User

from .models import Item


class ProductForm(forms.ModelForm):
    class Meta:
        model = Item
        fields = [
            'title',
            'Author',
            'Pub_Year',
            'Genre',
            'ISBN',
            'summary'

        ]


class UserForm(forms.ModelForm):
    class Meta:
        model = User
        fields = [
            'first_name',
            'last_name',
            'username',
            'email',
            'password',
        ]
