from django.contrib import admin

from .models import Item, Hold, Checkout

admin.site.register(Item)
admin.site.register(Hold)
admin.site.register(Checkout)
