import datetime
import json

from django.contrib.auth import authenticate, login
from django.contrib.auth.models import User
from django.contrib import messages
from django.http import HttpResponse
from django.shortcuts import render, get_object_or_404, redirect
from django.contrib.auth.models import User
from django.template.loader import select_template
from .forms import ProductForm, UserForm
from .models import Item, Hold, Checkout


# display login page
def log_in(request):
    print("tried to log in")
    if request.method == 'POST':
        print("should be logging")
        username = request.POST.get('username')
        password = request.POST.get('password')
        user = authenticate(username=username, password=password)
        if user:
            if user.is_active:
                login(request, user)
                if user.is_staff:
                    return redirect('admin_user')
                else:
                    return redirect('user')
    return render(request, 'registration/login.html')


def user_create_view(request):
    if request.method == 'POST':
        form = UserForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect('/create_user')
    else:
        form = UserForm()
    return render(request, 'create_user.html', context={'form': form})


def user_list_view(request):
    queryset = User.objects.all()
    context = {
        'object_List': queryset
    }
    return render(request, "user_list.html", context)


def user_detail_view(request, id):
    obj = User.objects.get(id=id)

    context = {
        'first_name': obj.first_name,
        'last_name': obj.last_name,
        'username': obj.username,
        'email': obj.email,
        # 'is_librarian': obj.is_staff()
    }
    return render(request, "user_details.html", context)


def user_delete_view(request, id):
    obj = get_object_or_404(User, id=id)
    if request.method == 'POST':
        obj.delete()
        return redirect('/Users/')

    context = {"object": obj}
    return render(request, "user_delete.html", context)


def product_delete_view(request, id):
    obj = get_object_or_404(Item, id=id)
    if request.method == 'POST':
        obj.delete()
        return redirect('/Items/')



    context = {"object": obj}
    return render(request, "inventory/product_delete.html", context)


def product_create_view(request):
    if request.method == 'POST':
        form = ProductForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect('/create')
    else:
        form = ProductForm()
    return render(request, 'inventory/item_create.html', {'form': form})


def product_detail_view(request, id):
    obj = Item.objects.get(id=id)

    context = {
        'title': obj.title,
        'Author': obj.Author,
        'Publication_Year': obj.Pub_Year,
        'Genre': obj.Genre,
        'ISBN': obj.ISBN,
        'description': obj.summary
    }
    return render(request, "inventory/details.html", context)


def product_list_view(request):
    queryset = Item.objects.all()
    context = {
        'object_List': queryset
    }
    return render(request,"inventory/product_list.html", context)


def get_holds_by_item_id(request):
    """Returns the number of active holds on a given item."""

    item_id = request.GET.get('item_id')
    if item_id is None:
        data = {
            'status': 'error',
            'reason': 'Request requires an item_id url parameter'
        }
        return HttpResponse(
            content=json.dumps(data),
            status=400
        )

    if not Item.objects.filter(id=item_id).exists():
        data = {
            'status': 'error',
            'reason': 'No item with the specified ID exists'
        }
        return HttpResponse(
            content=json.dumps(data),
            status=400
        )

    data = {
        'status': 'ok',
        'num_holds': Hold.objects.filter(
            item__id=item_id,
            released=False,
            fulfilled=False
        ).count()
    }

    return HttpResponse(content=json.dumps(data))


def order_view(request,id):
    obj = Item.objects.get(id=id)
    context = {
        'title': obj.title,
        'Author': obj.Author
    }

    if request.method == 'POST':
        username = request.POST.get('username')
        due_date = request.POST.get('due_date')

        Checkout.objects.create(
            item=obj,
            user=User.objects.get(username=username),
            due_date=datetime.datetime.strptime(due_date, '%Y-%m-%d').date()
        )
        return redirect('/Items')

    return render(request, 'inventory/order.html', context)


def receipt_history_view(request):
    queryset = Checkout.objects.all()
    context = {
        'object_List': queryset
    }
    return render(request, 'inventory/receipt.html', context)

"""need to see the https://docs.djangoproject.com/en/2.2/ref/contrib/auth/ for info of user."""

def order_conform(request):
    print("Order conforming...")
    if request.method == 'POST':
        cart = request.POST.get('cart')
        if cart is not None:
            new_item = Item(
                title="title",
                author="author"
            )
            if request.method == "POST":
                new_item.save()
                print("Order success.")
                return redirect('thank_you_page')
            else:
                messages.add_message(request, messages.ERROR, 'There is no item')
                return False
        else:
            messages.add_message(request, messages.ERROR, 'There is no item')
    return render(request, '/order.html')