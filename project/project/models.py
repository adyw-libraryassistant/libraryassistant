from datetime import date

from django.db import models
from django.contrib.auth.models import User


class Item(models.Model):
    title = models.CharField(max_length=140)
    Author = models.CharField(max_length=140)
    Pub_Year = models.IntegerField()
    Genre = models.CharField(max_length=120)
    ISBN = models.CharField(max_length=120)
    summary = models.TextField(blank = False, null = False, default= '')

    def __str__(self):
        return self.title

    def get_absolute_url(self):
        return f"/Items/{self.id}"

    @property
    def checked_out(self):
        """Returns True if the item is currently checked out, false otherwise."""
        return Checkout.objects.filter(
            item=self,
            returned=False
        ).exists()

    @property
    def active_holds(self):
        """Returns a QuerySet of active Holds on the item."""
        return Hold.objects.filter(item=self, fulfilled=False, released=False).all()


class Hold(models.Model):
    """Represents a hold by a user"""

    def __str__(self):
        formatted_time = self.creation_timestamp.strftime('%c')
        return f'{str(self.item)} held by {str(self.user)} on {formatted_time}'

    # The item being held
    item = models.ForeignKey(Item, on_delete=models.CASCADE)

    # The time the hold was placed
    creation_timestamp = models.DateTimeField(auto_now_add=True, blank=True)

    # The user who placed the hold
    user = models.ForeignKey(User, on_delete=models.CASCADE)

    # Whether the user has received the item
    fulfilled = models.BooleanField(default=False)

    # Whether the user has released the hold
    released = models.BooleanField(default=False)

    # INVARIANT: fulfilled and released are never both true.

    @property
    def active(self):
        """Whether the hold is still waiting to be fulfilled."""
        return not (self.fulfilled or self.released)

    completion_timestamp = models.DateTimeField(null=True, blank=True)


class Checkout(models.Model):
    """Represents the checkout of a single item."""

    def __str__(self):
        formatted_time = self.timestamp.strftime('%c')
        returned_str = 'Returned' if self.returned else 'Not returned'
        return (
            f'{self.item}, checked out on {formatted_time}'
            f' for user {self.user}. {returned_str}'
        )

    item = models.ForeignKey(
        Item,
        on_delete=models.CASCADE,
        related_name='checkouts'
    )

    timestamp = models.DateTimeField(
        'The time the item was checked out',
        auto_now_add=True,
        blank=True
    )

    returned = models.BooleanField(
        'Whether the item in the checkout has been returned',
        default=False
    )

    due_date = models.DateField(
        'The date the item is due to be returned by'
    )

    return_timestamp = models.DateTimeField(
        'The time when the item was returned',
        blank=True,
        null=True
    )

    # INVARIANT: if returned is false, then return_timestamp is null.
    # If returned is true, then return_timestamp is not null

    # The user who checked out the item
    user = models.ForeignKey(
        User,
        on_delete=models.CASCADE,
        related_name='librarian_checkouts'
    )

    @property
    def overdue(self):
        """True if the item is overdue, false otherwise"""
        return not self.returned and date.today() > self.due_date






