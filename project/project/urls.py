"""project URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.1/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path
from django.views.generic.base import TemplateView
from django.contrib.staticfiles.urls import static
from django.contrib.staticfiles.urls import staticfiles_urlpatterns
from . import views, settings
from .views import product_create_view, product_list_view, product_detail_view, product_delete_view, user_create_view, \
    user_delete_view, user_list_view, user_detail_view, order_view, receipt_history_view

urlpatterns = [
    path('admin/', admin.site.urls),
    path('', views.log_in, name='login'),
    path('administration', TemplateView.as_view(template_name='admin_user/index.html'), name="admin_user"),
    path('user', TemplateView.as_view(template_name='admin_user/basic_user.html'), name="user"),
    path('create_user/', user_create_view),
    path('Users/', user_list_view),  # TODO implement user_list_view
    path('Users/<int:id>/', user_detail_view),  # TODO implement user_detail_view
    path('Users/<int:id>/delete/', user_delete_view),
    path('create/', product_create_view, name='create'),
    path('Items/', product_list_view),
    path('Items/<int:id>/', product_detail_view),
    path('Items/<int:id>/delete/', product_delete_view),
    path('Items/<int:id>/checkout/', order_view),
    path('receipt', receipt_history_view, name="receipt"),

]

urlpatterns += staticfiles_urlpatterns()
urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
